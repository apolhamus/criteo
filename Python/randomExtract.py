#!/usr/bin/python

import random
import argparse
import csv

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("fpath", help="fpath of dataset")
    return parser.parse_args()

#..thanks to: http://stackoverflow.com/questions/10819911/read-random-lines-from-huge-csv-file-in-python
def main():
    args = arg_parser()	
    
    N = 500000
    N = N + 1
    fpath = args.fpath
    buffer = []

    f = open(fpath, 'r')
    for line_num, line in enumerate(f):
        n = line_num + 1.0
        r = random.random()
        if line_num == 0:
            addline = line.strip()
            heading = addline.split(",")
        if n <= N:
            addline = line.strip()
            buffer.append(addline.split(","))
        elif r < N/n:
            loc = random.randint(0, N-1)
            addline = line.strip()
            buffer[loc] = addline.split(",")
    
    print N, 'out of', n, 'total lines' 
    buffer[0] = heading
	
    with open("outfile.csv", "wb") as f:
        writer = csv.writer(f)
        writer.writerows(buffer)
					
if __name__ == "__main__":
    main()





